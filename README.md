Copyright (C) 2019 The LineageOS Project

Device configuration for Sony Xperia Z3 Compact Tablet LTE
=========================================

The Sony Xperia Z3 Compact Tablet LTE (codename _"scorpion"_) is a compact high-end tablet from Sony.

